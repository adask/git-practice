import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  doc,
  getDoc,
  getFirestore,
  collection,
  query,
  where,
  getDocs,
  setDoc,
  deleteDoc,
  deleteField,
  updateDoc,
  orderBy,
  limit,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";
//Inicjalizacja aplikacji
const firebaseConfig = {
  apiKey: "AIzaSyC5use8GLo51bSn6P55lIwmXtFo34i3Wyk",
  authDomain: "ogienbaza-59e37.firebaseapp.com",
  databaseURL:
    "https://ogienbaza-59e37-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "ogienbaza-59e37",
  storageBucket: "ogienbaza-59e37.appspot.com",
  messagingSenderId: "721597633007",
  appId: "1:721597633007:web:36544d26397f2e7522261a",
  measurementId: "G-DRTGH8S91Q",
};
const app = initializeApp(firebaseConfig);

const database = getFirestore(app);
const dbRef = doc(database, "pieski", "1");
const dbSnap = await getDoc(dbRef);

if (dbSnap.exists()) {
  console.log(dbSnap.data());
} else {
  console.log("error");
}
/* Dodawanie obiektow do bazy danych
dlaczego konsola wyswietla 'atrybuty' pieskow w roznej kolejnosci, mimo tego, ze sa okreslone tak samo w kodzie?
*/
await setDoc(doc(database, "pieski", "3"), {
  gryzie: false,
  rasa: "cocker spaniel",
  liczbaZabawek: "25",
  imie: "stokrotka",
});

await setDoc(doc(database, "pieski", "4"), {
  gryzie: true,
  rasa: "jamnik",
  liczbaZabawek: "1",
  imie: "pudzian",
});

await setDoc(doc(database, "pieski", "5"), {
  gryzie: true,
  rasa: "mops",
  liczbaZabawek: "4",
  imie: "winston",
});

/*usuwanie elementu kolekcji
await deleteDoc(doc(database, "pieski", "5"));
*/

/* usuwanie danej wlasciwosci - dlaczego usuwa mi dana wlasciwosc i po usunieciu linii kodu jej nie zwraca z powrotem?

await updateDoc(dbRef, {
  gryzie: deleteField(),
});

*/

//update dokumentu - ok
const mopsRef = doc(database, "pieski", "5");

await updateDoc(mopsRef, {
  imie: "jeff",
  liczbaZabawek: "8",
});
/* wyswietlenie calej kolekcji
const q = query(collection(database, "pieski"));

const qSnap = await getDocs(q);
qSnap.forEach((doc) => {
  console.log(doc.id, "szanowny piesek", doc.data());
});
*/

/*wyswietlenie elementow po danej wlasnosci
const querryGryzie = query(
  collection(database, "pieski"),
  where("gryzie", "==", true)
);
const gryzieSnap = await getDocs(querryGryzie);
gryzieSnap.forEach((doc) => {
  console.log(doc.id, "te pieski gryzom:(", doc.data());
});
*/

const q = query(collection(database, "pieski"), orderBy("imie"), limit(3));
const querrySnap = await getDocs(q);
querrySnap.forEach((pies) => {
  console.log(pies.data());
});

//modul Auth - zarejestrowac(e-mail + pw), zalogowac, wylogowac, obsluga sesji - formularz rejestracji pojawia sie, kiedy ktos jest niezarejestrowany, jesli ktos jest zalogowany, to pojawia sie przycisk wyloguj.
